# .SUFFIXES: .html .md
SRCS := $(wildcard markdown/*.md)
HTML := $(SRCS:markdown/%.md=%.html)
vpath %.md markdown
vpath %.html html

%.html: %.md
	pandoc --to=html5 --standalone -H ./templates/default.css --template=./templates/default.html --output=html/$@ $<

build: $(HTML)
