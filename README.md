# ottowao.com

## Portfolio website for @ottowao

*Making a build system for a single page site is probably overkill but I'll do it anyway*

## Tools

- [Pandoc][pandoc-ref] for markdown to html conversion

[pandoc-ref]: https://pandoc.org "Pandoc - a universal document converter"

## Inspiration and Resources

 - The KISS Linux [website][kiss-linux-ref] source and build process
 - Benji Fisher's presentation [generation system][benji-ref]

[kiss-linux-ref]: https://github.com/kisslinux/website "KISS Linux Website"
[benji-ref]: https://medium.com/isovera/devops-for-presentations-reveal-js-markdown-pandoc-gitlab-ci-34d07d2c1011 "DevOps for Presentations: Reveal.js, Markdown, Pandoc, GitLab CI"

